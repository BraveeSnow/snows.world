import axios from "axios";
import type { PageServerLoad } from "./$types";

interface GitlabCommit {
  project_id: number;
  created_at: string;
  push_data: {
    commit_to: string;
    commit_title: string;
    ref: string;
  };
}

async function getCommitHistory(): Promise<GitlabCommit[]> {
  try {
    return (
      await axios.get("https://gitlab.com/api/v4/users/6284484/events", {
        params: {
          action: "pushed",
        },
        headers: {
          Authorization: `Bearer ${import.meta.env.VITE_GITLAB_PAT}`,
        },
      })
    ).data.filter((commit: GitlabCommit) => commit.push_data.commit_title);
  } catch {
    return [];
  }
}

const commitCache = {
  refreshAt: Date.now() + 3_600_000,
  commits: await getCommitHistory(),
};

export const load: PageServerLoad = async () => {
  if (commitCache.refreshAt > Date.now()) {
    return {
      commits: commitCache.commits,
    };
  }

  commitCache.commits = await getCommitHistory();
  commitCache.refreshAt = Date.now() + 3_600_000;
  return {
    commits: commitCache.commits,
  };
};
