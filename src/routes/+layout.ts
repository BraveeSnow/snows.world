import "$lib/lang";

import { get } from "svelte/store";
import { locale, locales, waitLocale } from "svelte-i18n";
import { browser } from "$app/environment";
import type { LayoutLoad } from "./$types";

export const load: LayoutLoad = async () => {
  if (browser) {
    const storedLang = window.localStorage.getItem("i18n");
    if (storedLang && get(locales).includes(storedLang)) {
      locale.set(storedLang);
    } else if (get(locales).includes(window.navigator.language)) {
      locale.set(window.navigator.language);
    } else {
      locale.set("en");
    }
    locale.set(window.localStorage.getItem("i18n"));
  } else {
    locale.set("en");
  }

  await waitLocale();
};
