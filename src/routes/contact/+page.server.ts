import axios from "axios";
import type { Actions } from "./$types";
import { redirect } from "@sveltejs/kit";

const LAMBDA_SERVER = import.meta.env.VITE_LAMBDA_SERVER;

export const actions: Actions = {
  default: async ({ request }) => {
    const formEntries = (await request.formData()).entries();

    try {
      const res = (
        await axios.post(`${LAMBDA_SERVER}`, Object.fromEntries(formEntries), {
          headers: {
            "Content-Type": "application/json",
          },
        })
      ).data;

      if (res.error) {
        throw res.error;
      }
    } catch (err) {
      console.log(err);
      throw redirect(303, "/contact/submission?feedback=failure");
    }

    throw redirect(303, "/contact/submission?feedback=success");
  },
};
