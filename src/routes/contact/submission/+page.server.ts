import type { PageServerLoad } from "./$types";

export const load: PageServerLoad = async ({ url }) => {
  return {
    feedback: url.searchParams.get("feedback"),
  };
};
