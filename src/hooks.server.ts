import { locale, locales } from "svelte-i18n";
import { browser } from "$app/environment";
import type { Handle } from "@sveltejs/kit";
import { get } from "svelte/store";

export const handle: Handle = async ({ event, resolve }) => {
  const lang = event.request.headers.get("Accept-Language")?.split(",")[0].split("-")[0];
  if (lang && get(locales).includes(lang)) {
    locale.set(lang);
  }

  return resolve(event);
};
