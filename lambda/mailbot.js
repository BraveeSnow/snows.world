const sanitizeHtml = require("sanitize-html");
const validateEmail = require("validator/lib/isEmail");
const { SendEmailCommand, SESv2Client } = require("@aws-sdk/client-sesv2");

const sanitizerOptions = {
  disallowedTagsMode: "recursive",
};

const smtpClient = new SESv2Client({
  region: process.env.SMTP_REGION,
});

exports.handler = async function (event) {
  const payload = JSON.parse(event.body);

  if (!(payload.name && payload.email && validateEmail(payload.email) && payload.message)) {
    return {
      statusCode: 400,
      isBase64Encoded: false,
      headers: {
        "Content-Type": "application/json",
      },
      body: '{ error: "Bad request" }',
    };
  }

  const sanitizedName = sanitizeHtml(payload.name, sanitizerOptions);
  const sanitizedOrg = sanitizeHtml(payload.org || "Unaffiliated", sanitizerOptions);
  const sanitizedMessage = sanitizeHtml(payload.message, sanitizerOptions);

  // Email sent to my inbox
  const notification = new SendEmailCommand({
    FromEmailAddress: process.env.SMTP_ADDRESS,
    Destination: {
      ToAddresses: [process.env.SMTP_SENDTO],
    },
    Content: {
      Simple: {
        Subject: {
          Data: `Contact Request - ${sanitizedName}`,
        },
        Body: {
          Html: {
            Data: `<b>New Contact Request</b>
              <p><b>Name</b>: ${sanitizedName}</p>
              <p><b>Email</b>: ${payload.email}</p>
              <p><b>Organization</b>: ${sanitizedOrg}</p>
              <p><b>Message</b> is attached below:</p>
              <blockquote>
                ${sanitizedMessage}
              </blockquote>`,
          },
        },
      },
    },
  });

  // Email sent to requester
  const forward = new SendEmailCommand({
    FromEmailAddress: process.env.SMTP_ADDRESS,
    Destination: {
      ToAddresses: [payload.email],
    },
    Content: {
      Simple: {
        Subject: {
          Data: "snows.world - Contact Confirmation",
        },
        Body: {
          Html: {
            Data: `<p>Dear ${sanitizedName},</p>
            <p>Thanks for contacting me! This is an automated message to confirm your message has been forwarded successfully.</p>
            <p>Below is the message forwarded for reference:</p>
            <blockquote>
              ${sanitizedMessage}
            </blockquote>
            <p>Looking forward to follow up with you.</p>
            <br />
            <p>Kind regards,</p>
            <i>David Long</i>`,
          },
        },
      },
    },
  });

  try {
    await smtpClient.send(notification);
    await smtpClient.send(forward);
  } catch (err) {
    console.error(err);
    return {
      statusCode: 500,
      isBase64Encoded: false,
      headers: {
        "Content-Type": "application/json",
      },
      body: '{ error: "Internal server error" }',
    };
  }

  return {
    statusCode: 200,
    isBase64Encoded: false,
    headers: {
      "Content-Type": "application/json",
    },
    body: "{}",
  };
};
